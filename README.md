# angular-ui-router-anim-in-out

An animation directive to use with ngAnimate 1.2+ and ui-router


## Installation

`$ bower install angular-ui-router-anim-in-out --save`


## Compile Sass (requires Bourbon)

`$ sass scss/anim-in-out.scss css/anim-in-out.css`


## Demo

http://homerjam.github.io/angular-ui-router-anim-in-out/
